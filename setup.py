# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

with open('requirements.txt') as f:
    requirements = [i.strip() for i in f.readlines()]

setup(
    name='uberterm',
    version='0.0.1',
    description='Terminal app for Ubertown Camp at Nowhere Festival',
    long_description=readme,
    author='Rich King',
    author_email='kingamajick@gmail.com',
    url='https://github.com/kingamajick/uberterm',
    license=license,
    install_requires=requirements,
    packages=find_packages(exclude=('tests', 'docs'))
)
