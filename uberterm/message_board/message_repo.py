import sqlite3

import logging

from message_board.models.message import Message
from message_board.models.message_thread import MessageThread

__log = logging.getLogger(__name__)

__threads = {}
__db_connection = None


def initialise(location):
    __log.info("Init Messages DB {0}".format(location))

    global __db_connection
    __db_connection = sqlite3.connect(location)
    c = __db_connection.cursor()

    c.execute('CREATE TABLE IF NOT EXISTS threads (id GUID, title TEXT)')
    c.execute(
        'CREATE TABLE IF NOT EXISTS messages (id GUID, thread_id GUID, author TEXT, content TEXT, timestamp INTEGER)')

    __db_connection.commit()

    for row in c.execute('''SELECT threads.id, threads.title, messages.id, messages.author, messages.content, messages.timestamp
                FROM threads
                INNER JOIN messages
                ON threads.id = messages.thread_id
                ORDER BY messages.timestamp'''):
        thread = get_thread(row[0])

        if not thread:
            thread = MessageThread(row[1],
                                   Message(row[3], row[4], row[0], row[2], row[5]), id_=row[0])
            __threads[thread.id_] = thread
            __log.debug("Created new thread {0}".format(thread))
        else:
            thread.messages.append(Message(row[3], row[4], row[0], row[2], row[5]))

            __log.debug("Added message tp thread {0}".format(thread.id_))


def shutdown():
    __log.info("Shutdown Messages DB")

    global __db_connection
    __db_connection.close()


def get_threads():
    """
    :return: all threads stored in the repo.
    """
    return list(__threads.values())


def get_thread(id_):
    """
    Get a thread for the given id.
    :param id_: the id of the thread to return.
    :return: the thread, or None if no such thread exits.
    """
    return __threads.get(id_, None)


def add_thread(title, first_message):
    """
    Adds a new thread to the repository and returns it's id.
    :param title: title of the message.
    :param first_message: the first message in thread.
    :return: thread id.
    """

    new_thread = MessageThread(title, first_message)
    __threads[new_thread.id_] = new_thread
    __add_thread_db(new_thread)

    return new_thread.id_


def add_message_to_thread(id_, message):
    """
    Adds a message to an existing thread
    :param id_: the id of the thread to add too.
    :param message: the message to append to the thread.
    """
    thread = get_thread(id_)
    if not thread:
        raise ValueError("Unknown thread id {0}".format(id_))
    message.thread_id = id_
    __add_message_to_db(message)
    thread.messages.append(message)


def __add_thread_db(new_thread):
    __log.debug("Write thread to DB")

    first_message = new_thread.messages[0]

    c = __db_connection.cursor()
    c.execute('INSERT INTO threads VALUES (?, ?)', (str(new_thread.id_), new_thread.title))
    c.execute('INSERT INTO messages VALUES (?, ?, ?, ?, ?)',
              (str(first_message.id_), str(new_thread.id_), first_message.author,
               first_message.content, first_message.timestamp))
    __db_connection.commit()


def __add_message_to_db(message):
    __log.debug("Write message to DB")
    c = __db_connection.cursor()
    c.execute('INSERT INTO messages VALUES (?, ?, ?, ?, ?)', (str(message.id_), str(message.thread_id), message.author,
                                                              message.content, message.timestamp))
    __db_connection.commit()


def __check_db_connection():
    if not __db_connection:
        raise SystemError("DB hasn't been initialised")
