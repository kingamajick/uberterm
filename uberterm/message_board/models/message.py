import time
import uuid


class Message:
    def __init__(self, author, content, thread_id=None, id_=None, timestamp=None):
        self.author = author
        self.content = content
        self.thread_id = thread_id
        self.id_ = id_ if id_ else uuid.uuid1()
        self.timestamp = timestamp if timestamp else int(time.time() * 1000)
