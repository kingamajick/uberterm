import uuid


class MessageThread:
    def __init__(self, title, message, id_=None):
        self.title = title
        self.messages = [message]
        self.id_ = id_ if id_ else uuid.uuid1()

    def get_last_updated(self):
        return self.messages[-1].timestamp
