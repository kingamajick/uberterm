import logging

import urwid
from urwid import Button, Frame, Pile, Divider, Padding, Text, CENTER, SimpleFocusListWalker, ListBox, RELATIVE, AttrMap
from util.ui.view import View

from message_board import message_repo
from util.ui.presenter import Presenter
from widgets.header import Header
from widgets.widget_util import hide_button_cursor


class MessageBoardMenuPresenter(Presenter):
    log = logging.getLogger(__name__)

    def __init__(self, view, flow):
        super().__init__(view, flow)
        view.set_on_option_selected(self.on_option_selected)

    def on_start(self):
        if not message_repo.get_threads():
            pass

    def on_option_selected(self, option):
        self.log.debug("Option {0} chosen".format(option))
        if option == 0:
            self.flow.launch_message_board_message_list()
        elif option == 1:
            self.flow.launch_message_new_message()
        else:
            raise ValueError("Option {0} not found".format(option))


TITLE = """
Welcome to Übertown message board citizen.  Here you can leave messages for your fellow citizens.  For your protection 
all messages will be made available under the Investigatory Powers Act 2016.  
""".replace("\n", '')  # Remove new lines

MENU = [
    '1. View Messages',
    '2. Create message',
]


class MessageBoardMenuView(View):
    __on_option_selected_callback = None

    def __init__(self, main_loop):
        super().__init__(main_loop)

    def set_on_option_selected(self, callback):
        """
        Set a callback
        :param callback:
        :return:
        """
        self.__on_option_selected_callback = callback

    def _create_body(self):
        return Frame(
            header=Header(TITLE),
            body=Padding(ListBox(SimpleFocusListWalker(self.__get_list_content())),
                         width=(RELATIVE, 80), align=CENTER)
        )

    def _create_footer(self):
        return Text('Use the arrow keys (↑, ↓)  to select an option then press <Enter>', align='center')

    def __get_list_content(self):
        content = []
        for i in range(0, len(MENU)):
            button = Button(MENU[i])
            hide_button_cursor(button)
            button.index = i

            def handler(widget):
                if self.__on_option_selected_callback is not None:
                    self.__on_option_selected_callback(widget.index)

            urwid.connect_signal(button, 'click', handler)
            content.append(AttrMap(button, None, focus_map='content_inverse'))
        return content
