import logging
import time

import urwid
from urwid import Frame, Pile, Divider, Padding, Text, SimpleFocusListWalker, ListBox, Columns, AttrMap, Filler
from util.ui.view import View

from message_board import message_repo as repo
from util.ui.presenter import Presenter
from widgets import widget_util
from widgets.header import Header
from widgets.plain_button import PlainButton


class MessageListPresenter(Presenter):
    log = logging.getLogger(__name__)

    def __init__(self, view, flow):
        super().__init__(view, flow)
        view.on_message_thread_selected = self.on_message_thread_selected

    def on_start(self):
        self.view.threads = sorted(repo.get_threads(), key=lambda t: t.get_last_updated(), reverse=True)

    def handle_input(self, key):
        if super().handle_input(key):
            return True

        if key == 'r' or key == 'R':
            self.flow.launch_message_new_message()
            return True

    def on_message_thread_selected(self, thread):
        self.log.debug("Thread {0} chosen".format(thread))
        self.flow.launch_message_board_thread(thread.id_)
        pass


TITLE = "Übertown messages"
MAX_TITLE_LEN = 69


class MessageListView(View):
    on_message_thread_selected = None
    threads = []

    def __init__(self, main_loop):
        super().__init__(main_loop)

    def _create_body(self):
        if self.threads:
            body = ListBox(SimpleFocusListWalker(self.__get_list_content()))
        else:
            body = Filler(Pile([
                Text("No threads, maybe you should write one??", align='center'),
                Divider(),
                Text("Press <R> to create a new thread", align='center')
            ]))

        return Frame(
            header=Header(TITLE),
            body=body,
            footer=Divider()
        )

    def _create_footer(self):
        return Text('To select an thread press <Enter>, to create a new thread press <R>',
                    align='center')

    def __get_list_content(self):
        content = []

        for thread in self.threads:

            if len(thread.title) > MAX_TITLE_LEN:
                title_text = thread.title[:MAX_TITLE_LEN].rstrip() + '...'
            else:
                title_text = thread.title
            title = PlainButton(title_text)
            widget_util.hide_button_cursor(title)
            count = Text(str(len(thread.messages)))

            formatted_last_update = time.strftime('%a %d %H:%M', time.localtime(thread.get_last_updated() / 1000))
            view = Columns([('fixed', 12, Text(formatted_last_update)),
                            ('weight', 1, title),
                            ('fixed', 3, count)])

            title.thread = thread

            def handler(widget):
                if self.on_message_thread_selected is not None:
                    self.on_message_thread_selected(widget.thread)

            urwid.connect_signal(title, 'click', handler)
            content.append(AttrMap(view, None, focus_map='content_inverse'))

        return content
