import logging
from collections import deque

import urwid
from urwid import Pile, Padding, Text, SimpleFocusListWalker, ListBox, Columns, WidgetPlaceholder, LineBox, Edit, \
    Button, AttrMap
from util.ui.view import View

from message_board import message_repo
from message_board.models.message import Message
from util.ui.presenter import Presenter
from widgets import widget_util
from widgets.fixed_edit import FixedEdit
from widgets.listbox_with_scrollbars import ListBoxWithScrollBars


class MessageReplyPresenter(Presenter):
    __l = logging.getLogger(__name__)

    def __init__(self, view, flow, thread_id=None):
        super().__init__(view, flow)
        self.thread_id = thread_id
        if not thread_id:
            view.has_title = True
        view.on_reply = self._on_post_message

    def handle_input(self, key):
        if super().handle_input(key):
            return True

        if key == 'tab':
            self.view.move_to_next_focus()
            return True
        if key == 'shift tab':
            self.view.move_to_previous_focus()
            return True
        if key == 'enter':
            self.view.move_to_next_focus()
            return True

    def _on_post_message(self, title, message):
        title_errors = self._validate_title(title)
        message_errors = self._validate_message(message)

        if title_errors or message_errors:
            self.view.show_errors(title_errors + message_errors)

        # Posting in existing thread
        elif self.thread_id:
            message_repo.add_message_to_thread(self.thread_id, message)
            self.flow.back()

        # Creating new thread
        else:
            thread_id = message_repo.add_thread(title, message)
            self.flow.launch_message_board_message_list()

    def _validate_title(self, title):
        if self.thread_id or title:
            return []
        else:
            return ["A thread must have a title nitwit!"]

    @staticmethod
    def _validate_message(message):
        errors = []
        if not message.author:
            errors.append("An author must have a name")
        if not message.content:
            errors.append("What is a message without a message, nothing")
        return errors


class NewMessageView(View):
    __l = logging.getLogger(__name__)
    has_title = False
    on_reply = None

    def __init__(self, main_loop):
        super().__init__(main_loop)

        self.title = FixedEdit(max_length=100)
        self.author = FixedEdit(max_length=25)
        self.content = Edit(multiline=True)

        self.reply_button = Button('Reply')
        widget_util.hide_button_cursor(self.reply_button)
        urwid.connect_signal(self.reply_button, 'click', self.handle_reply_click)

    def show_errors(self, errors):
        self._errors.show_errors(errors)

    def clear_errors(self):
        self._errors.clear_errors()

    """
    It's a pain to have to do all this focus stuff manually, as there doesn't seem to be an easy way to handle wanting
    to be able to shift the focus back and forward to the next widget.  This is all pretty fragile...
    """

    def move_to_next_focus(self):
        reply_container_focus, body_focus = self.__get_next_focus()
        self.reply_container.set_focus_path(reply_container_focus)
        self.body.set_focus_path(body_focus)

        # ListBox doesn't invalidate when it's focus is changed  ¯\_(ツ)_/¯
        self.reply_container._invalidate()

    def move_to_previous_focus(self):
        reply_container_focus, body_focus = self.__get_previous_focus()
        self.reply_container.set_focus_path(reply_container_focus)
        self.body.set_focus_path(body_focus)

        # ListBox doesn't invalidate when it's focus is changed  ¯\_(ツ)_/¯
        self.reply_container._invalidate()

    def __get_next_focus(self):
        body_focus = self.body.get_focus_path()
        reply_container_focus = self.reply_container.get_focus_path()
        if body_focus[0] == 2:
            # Keep the focused in the list_box to message, but change the body focus to reply
            return reply_container_focus, body_focus

        if len(reply_container_focus) == 1:
            # We're focused on the message section, so now move to the reply button
            return reply_container_focus, [2]

        if reply_container_focus[0] == 1:
            # We must have a title and focus on the author field, move to message
            assert self.has_title
            return [3], [1]

        if self.has_title:
            return [1, 1], [1]
        else:
            return [2], [1]

    def __get_previous_focus(self):
        body_focus = self.body.get_focus_path()
        reply_container_focus = self.reply_container.get_focus_path()
        if body_focus[0] == 2:
            # Moving from reply to message field
            return [3 if self.has_title else 2], [1]

        if len(reply_container_focus) == 1:
            # We're focused on the message section, so now move to the section above
            return [reply_container_focus[0] - 2, 1], [1]

        if reply_container_focus[0] == 0:
            # Can't go back further, so keep focus
            return reply_container_focus, body_focus

        return [reply_container_focus[0] - 1, 1], [1]

    def _create_body(self):
        # A note to future me, if you are changing stuff here, remember you probably need to change the next and
        # previous focus logic in this class too.
        self._errors = self.ErrorSwitcher()
        fields = []

        if self.has_title:
            fields.append(Columns([(urwid.PACK, Text("Title: ")), (urwid.WEIGHT, 1, self.title)]))

        fields.extend([
            Columns([(urwid.PACK, Text("Author: ")), (urwid.WEIGHT, 1, self.author)]),
            Text("Message:"),
            self.content
        ])

        self.reply_container = ListBox(SimpleFocusListWalker(fields))
        return Pile([
            (urwid.FLOW, self._errors),
            LineBox(ListBoxWithScrollBars.from_list_box(self.reply_container), title='Reply'),
            (urwid.FLOW, Padding(
                AttrMap(self.reply_button, None, focus_map='content_inverse'),
                align=urwid.RIGHT, width=len(self.reply_button.get_label()) + 4))
        ])

    def _create_footer(self):
        return Text('Use the arrow keys (↑, ↓)  to browse the fields, <ESC> to go back',
                    align='center')

    def handle_reply_click(self, widget):
        if self.on_reply:
            msg = Message(self.author.edit_text, self.content.edit_text.strip())
            self.on_reply(self.title.edit_text, msg)

    class ErrorSwitcher(WidgetPlaceholder):
        __empty = Pile([])

        def __init__(self):
            super().__init__(self.__empty)

        def show_errors(self, errors):
            widgets = []
            for error in errors:
                widgets.append(Text(error))

            self.original_widget = Pile(widgets)

        def clear_errors(self):
            self.original_widget = self.__empty
