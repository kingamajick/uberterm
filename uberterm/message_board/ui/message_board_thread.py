import logging
import time

from urwid import Frame, Divider, Text, SimpleFocusListWalker, ListBox, Columns
from util.ui.view import View

from message_board import message_repo
from util.ui.presenter import Presenter
from widgets.listbox_with_scrollbars import ListBoxWithScrollBars


class MessageThreadPresenter(Presenter):
    log = logging.getLogger(__name__)

    def __init__(self, view, flow, thread_id):
        super().__init__(view, flow)
        self.thread_id = thread_id

    def on_start(self):
        thread = message_repo.get_thread(self.thread_id)
        self.view.set_message_thread(thread)

    def handle_input(self, key):
        if super().handle_input(key):
            return True

        if key == 'r' or key == 'R':
            self.flow.launch_message_board_reply(self.thread_id)
            return True


class MessageThreadView(View):
    __on_message_thread_selected = None
    __message_thread = None

    def __init__(self, main_loop):
        super().__init__(main_loop)

    def set_message_thread(self, message_thread):
        self.__message_thread = message_thread

    def _create_body(self):
        return Frame(
            header=Divider(),
            body=ListBoxWithScrollBars.from_list_box(ListBox(SimpleFocusListWalker(self.__get_list_content()))),
            footer=Divider()
        )

    def _create_footer(self):
        return Text('Use the arrow keys (↑, ↓)  to browse the thread, <R> to reply, <ESC> to go back', align='center')

    def __get_list_content(self):
        content = []

        content.append(Text("Title:")),
        content.append(Text(self.__message_thread.title, align='left'))
        content.append(Divider())

        for i in range(0, len(self.__message_thread.messages)):
            message = self.__message_thread.messages[i]
            formatted_post_time = time.strftime('%a %d %H:%M', time.localtime(message.timestamp / 1000))

            post_time = Text(formatted_post_time)

            message_text = Text(message.content)

            author = Text(message.author, align='right')

            thread_title = Columns([('weight', 1, post_time),
                                    ('weight', 1, author)])

            content.append(thread_title)
            content.append(message_text)

            if i == len(self.__message_thread.messages) - 1:
                content.append(Text("----- End Thread", align='left'))
            else:
                content.append(Text("-----", align='left'))

        return content
