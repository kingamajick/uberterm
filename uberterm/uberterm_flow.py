import logging
import os
import sys

from urwid import ExitMainLoop

from census.census_details import CensusDetailsView, CensusDetailsPresenter
from census.census_list import CensusView, CensusPresenter
from directory_viewer.directory_renderer import DirectoryRendererView, DirectoryRendererPresenter
from directory_viewer.file_renderer import FileRendererView, FileRendererPresenter
from landing import LandingPresenter, LandingView
from menu import MenuView, MenuPresenter
from message_board.ui.message_board_list import MessageListView, MessageListPresenter
from message_board.ui.message_board_menu import MessageBoardMenuView, MessageBoardMenuPresenter
from message_board.ui.message_board_new_message import NewMessageView, MessageReplyPresenter
from message_board.ui.message_board_thread import MessageThreadView, MessageThreadPresenter
from test import TestView, TestPresenter


class UbertermFlow:
    def __init__(self, main_loop, config_dir):
        self.__l = logging.getLogger(self.__class__.__name__)
        self.main_loop = main_loop
        self.config_dir = config_dir
        self.__back_stack = []

    def finished(self):
        self.back()

    def launch_test(self):
        self.__l.debug("Launching test screen")
        v = TestView(self.main_loop)
        self._move_to(TestPresenter(v, self))

    def initial(self):
        self.landing()
        #self.launch_test()
        # self.launch_directory(os.path.join(config_dir, "records"), "records")
        # self.launch_message_board_menu()

    def restart(self):
        self.__back_stack.clear()
        self.initial()

    def landing(self):
        self.__l.debug("Launching initial screen")
        v = LandingView(self.main_loop)
        self._move_to(LandingPresenter(v, self))

    def launch_menu(self):
        self.__l.debug("Launching menu")
        v = MenuView(self.main_loop)
        self._move_to(MenuPresenter(v, self))

    # Census

    def launch_census_list(self):
        self.__l.debug("Launching census entries")
        v = CensusView(self.main_loop)
        self._move_to(CensusPresenter(v, self))

    def launch_census_details(self, uid):
        self.__l.debug("Launching census details for {0}".format(uid))
        v = CensusDetailsView(self.main_loop)
        self._move_to(CensusDetailsPresenter(v, self, uid))

    # End Census

    # Message Board

    def launch_message_board_menu(self):
        self.__l.debug("Launching message board menu")
        v = MessageBoardMenuView(self.main_loop)
        self._move_to(MessageBoardMenuPresenter(v, self))

    def launch_message_board_message_list(self):
        self.__l.debug("Launching message board message list")
        v = MessageListView(self.main_loop)
        self._move_to(MessageListPresenter(v, self))

    def launch_message_board_thread(self, thread_id):
        self.__l.debug("Launching message board message list")
        v = MessageThreadView(self.main_loop)
        self._move_to(MessageThreadPresenter(v, self, thread_id))

    def launch_message_new_message(self):
        self.__l.debug("Launching message board new message")
        v = NewMessageView(self.main_loop)
        self._move_to(MessageReplyPresenter(v, self), False)

    def launch_message_board_reply(self, thread_id):
        self.__l.debug("Launching message board reply")
        v = NewMessageView(self.main_loop)
        self._move_to(MessageReplyPresenter(v, self, thread_id))

    # End Message Board

    # Directory and File rendering

    def launch_archives(self):
        self.launch_directory(os.path.join(self.config_dir, 'archives'), 'archives')

    def launch_bar_menu(self):
        self.launch_file(os.path.join(self.config_dir, 'barmenu'), 'Bar Menu', align='center')

    def launch_help(self):
        self.launch_file(os.path.join(self.config_dir, 'help'), 'Help')

    def launch_directory(self, path, title):
        self.__l.debug("Launching directory {0}:{1}".format(title, path))
        v = DirectoryRendererView(self.main_loop)
        self._move_to(DirectoryRendererPresenter(path, title, v, self))

    def launch_file(self, path, title, align='left'):
        self.__l.debug("Launching file {0}:{1}".format(title, path))
        v = FileRendererView(self.main_loop, align)
        self._move_to(FileRendererPresenter(path, title, v, self))

    # End Directory and File rendering

    def forward_input(self, key):
        if len(self.__back_stack) > 0:
            top = self.__back_stack[-1]
            self.__l.debug("Forwarding input {0} to {1}".format(key, top['presenter']))
            return top['presenter'].handle_input(key)
        return False

    def forward_inactivity(self):
        if len(self.__back_stack) > 0:
            top = self.__back_stack[-1]
            self.__l.debug("Forwarding inactivity to {0}".format(top['presenter']))
            top['presenter'].handle_inactivity()

    def back(self):
        if len(self.__back_stack) > 1:
            current_stack_entry = self.__back_stack.pop()
            next_stack_entry = self.__back_stack[-1]
            self.__l.debug(
                "Moving back from {0} to {1}".format(current_stack_entry['presenter'], next_stack_entry['presenter']))
            UbertermFlow.finish(current_stack_entry)
            UbertermFlow.start(next_stack_entry)
        else:
            raise ExitMainLoop()

    def _move_to(self, next_presenter, back_stack=True):
        if len(self.__back_stack) > 0:
            current_stack_entry = self.__back_stack[-1]
            UbertermFlow.finish(current_stack_entry)
            if not current_stack_entry['back_stack']:
                self.__back_stack.pop()

        new_stack_entry = {'presenter': next_presenter, 'back_stack': back_stack}
        self.__back_stack.append(new_stack_entry)
        UbertermFlow.start(new_stack_entry)

    @staticmethod
    def start(stack_entry):
        stack_entry['presenter'].on_start()
        stack_entry['presenter'].view.render()

    @staticmethod
    def finish(stack_entry):
        stack_entry['presenter'].view.destroy()
        stack_entry['presenter'].on_stop()
