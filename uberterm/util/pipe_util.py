import os


def publish(pipe, value):
    os.write(pipe, encode_int(value))


def wrapped_handler(handler):
    def wrapped(value):
        handler(decode_int(value))

    return wrapped


def encode_int(value):
    return value.to_bytes(((value.bit_length() + 7) // 8), byteorder='big')


def decode_int(value):
    return int.from_bytes(value, byteorder='big')
