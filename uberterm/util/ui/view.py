import logging

import urwid
from urwid import Button, Frame, AttrMap, Filler, Padding, Columns, Text, CENTER, SolidFill, PopUpLauncher, WidgetWrap, \
    LineBox, BoxAdapter, Divider

_TITLE = 'Übertown central terminal'
_VERSION = 'v0.02'
_DEFAULT_FOOTER = 'Press <ESC> to go back'


class PopUpDialog(WidgetWrap):
    signals = ['positive', 'negative']

    handler = None

    def __init__(self, main_loop, message, positive_text, negative_text=None, auto_positive_in=None):
        self.main_loop = main_loop

        content = [Divider(), urwid.Text(message)]

        buttons = []

        PopUpDialog.append_spacer(buttons)

        if not auto_positive_in:
            positive_button = Button(positive_text)
        else:
            positive_button = Button("{0} ({1}s)".format(positive_text, auto_positive_in))
        urwid.connect_signal(positive_button, 'click', self._on_positive)
        buttons.append((len(positive_button.get_label()) + 4, positive_button))

        PopUpDialog.append_spacer(buttons)

        if negative_text:
            negative_button = Button(negative_text)
            urwid.connect_signal(negative_button, 'click', self._on_negative)
            buttons.append((len(negative_text) + 4, negative_button))

            PopUpDialog.append_spacer(buttons)

        content.append(Divider())
        content.append(Columns(buttons))

        view = AttrMap(
            Filler(
                Padding(
                    LineBox(
                        Padding(urwid.Pile(content),
                                width=41, align='center'),
                        title="Attention"),
                    width=('relative', 80), align='center')
            ),
            'popup')

        if auto_positive_in:
            def on_tick(__, user_data):
                if user_data == 0:
                    self._on_positive(None)
                else:
                    positive_button.set_label("{0} ({1}s)".format(positive_text, user_data))
                    self.handler = main_loop.set_alarm_in(1, on_tick, user_data - 1)

            self.handler = main_loop.set_alarm_in(1, on_tick, auto_positive_in)

        super().__init__(view)

    def _on_positive(self, source):
        if self.handler:
            self.main_loop.remove_alarm(self.handler)

        self._emit("positive")

    def _on_negative(self, source):
        if self.handler:
            self.main_loop.remove_alarm(self.handler)

        self._emit("negative")

    @staticmethod
    def append_spacer(items):
        items.append(('weight', 1, BoxAdapter(SolidFill(" "), 1)))


class _PopupHandler(PopUpLauncher):
    message = None
    positive_text = None
    positive_action = None
    auto_positive_in = None
    negative_text = None
    negative_action = None

    def __init__(self, main_loop, widget):
        super().__init__(widget)
        self._main_loop = main_loop

    def create_pop_up(self):
        pop_up = PopUpDialog(self._main_loop, self.message,
                             auto_positive_in=self.auto_positive_in,
                             positive_text=self.positive_text,
                             negative_text=self.negative_text)
        urwid.connect_signal(pop_up, 'positive',
                             lambda button: self.positive_action())

        urwid.connect_signal(pop_up, 'negative',
                             lambda button: self.negative_action())
        return pop_up

    def get_pop_up_parameters(self):
        return {'left': 0, 'top': 1, 'overlay_width': ('relative', 100), 'overlay_height': 16}

    def setup(self, message, positive_text, positive_action, auto_positive_in=None, negative_text=None,
              negative_action=None):
        self.message = message
        self.positive_text = positive_text
        self.positive_action = positive_action
        self.auto_positive_in = auto_positive_in
        self.negative_text = negative_text
        self.negative_action = negative_action

    def reset(self):
        self.message = None
        self.positive_text = None
        self.positive_action = None
        self.auto_positive_in = None
        self.negative_text = None
        self.negative_action = None

    @property
    def showing_popup(self):
        return self._pop_up_widget is not None

    @property
    def base_widget(self):
        return self._original_widget


# 56 : max ▶
# 16 : max ▼
class View(object):
    def __init__(self, main_loop):
        self._l = logging.getLogger(self.__class__.__name__)
        self._main_loop = main_loop

    def render(self):
        """
        Render the view using the current
        """
        self._l.debug("render view")
        self._main_loop.widget = _PopupHandler(self._main_loop,
                                               Frame(
                                                   header=AttrMap(self._create_header(), 'header'),
                                                   body=AttrMap(
                                                       Filler(Padding(self._create_body(), 'center', 56), 'top', 16),
                                                       'content'),
                                                   footer=AttrMap(Padding(self._create_footer(), left=2, right=2),
                                                                  'footer')
                                               )
                                               )

    def destroy(self):
        """
        Clean up before the view is disposed of
        """
        self._l.debug("destroy view")
        pass

    def show_pop_up(self, message, positive_text, positive_action, auto_positive_in=None, negative_text=None,
                    negative_action=None):
        if self._main_loop.widget.showing_popup:
            self.close_pop_up()

        self._l.debug("Show popup")
        self._main_loop.widget.setup(message, positive_text, positive_action,
                                     auto_positive_in=auto_positive_in,
                                     negative_text=negative_text,
                                     negative_action=negative_action)
        self._main_loop.widget.open_pop_up()

    def close_pop_up(self):
        self._l.debug("Close popup")
        self._main_loop.widget.close_pop_up()
        self._main_loop.widget.reset()

    @property
    def screen(self):
        return self._main_loop.widget.base_widget

    @property
    def header(self):
        return self.screen.get_header()

    @property
    def body(self):
        # Actually body is inside a Filler and Padding
        return self.screen.get_body().original_widget.original_widget

    @property
    def footer(self):
        # Actually footer is inside a Filler and Padding
        return self.screen.get_footer().original_widget

    def _create_header(self):
        return Columns([
            ('fixed', 7, Text('')),
            ('weight', 1, Text(self._get_title(), align=CENTER)),
            ('fixed', 7, Text(_VERSION))])

    def _get_title(self):
        return _TITLE

    def _create_body(self):
        """
        Implement this method to return the body of the view, which will be placed between the footer and the header
        """
        raise NotImplemented()

    def _create_footer(self):
        return Text(_DEFAULT_FOOTER, align='center')
