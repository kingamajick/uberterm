import logging


class Presenter(object):
    def __init__(self, view, flow):
        self._l = logging.getLogger(self.__class__.__name__)
        self.view = view
        self.flow = flow

    def handle_input(self, key):
        """
        Handle any keys pressed by the user while this presenter is active.
        :return: True if the key press was handled by the presenter
        """
        if key == "ctrl c":
            # self, message, positive_text, positive_action, negative_text = None, negative_action = None
            self.view.show_pop_up("Trying to exit hey?  We'll you can go back to the beginning, but you can't escape",
                                  "Start new session",
                                  self.flow.restart,
                                  negative_text="Cancel",
                                  negative_action=self.view.close_pop_up)
            return True
        return False

    def handle_inactivity(self):
        self.view.show_pop_up("Are you tired Übertard? or have you simply abandoned your post?",
                              "Start new session",
                              self.flow.restart,
                              auto_positive_in=4,
                              negative_text="Cancel",
                              negative_action=self.view.close_pop_up, )

    def on_start(self):
        pass

    def on_stop(self):
        pass
