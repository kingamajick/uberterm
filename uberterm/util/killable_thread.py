import threading


class KillableThread(threading.Thread):
    _kill = False

    def __init__(self):
        super().__init__()

    def kill(self):
        self._kill = True
