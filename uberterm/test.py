import urwid
from urwid import Text, Filler, Pile, Padding
from util.ui.view import View

from util.ui.presenter import Presenter
from widgets.type_writer_text import TypeWriterText

TEST = """
Unauthorised access to the Überterm is strictly and absolutely forbidden.

Unauthorised access will involve having to fill in some forms in triplicate, at least one shot, and potentially death.
"""
# 56 : max ▶
# 16 : max ▼
FIT = """*------------------------------------------------------*
| Test Frame                                           |
|                                                      |
|                                                      |
|                                                      |
|                                                      |
|                                                      |
|                                                      |
|                                                      |
|                                                      |
|                                                      |
|                                                      |
|                                                      |
*------------------------------------------------------*
"""


class TestPresenter(Presenter):
    def __init__(self, view, flow):
        super().__init__(view, flow)

    def handle_input(self, key):
        if key == 'enter':
            self.view.show_pop_up(
                "Unauthorised access will involve having to fill in some forms in triplicate, at least"
                " one shot, and potentially death.",
                "OK", self.view.close_pop_up,
                negative_text="Cancel", negative_action=self.view.close_pop_up)


class TestView(View):
    def __init__(self, main_loop):
        super().__init__(main_loop)

    def _create_body(self):
        return Filler(Text(FIT))
