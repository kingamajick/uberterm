import logging
import os
import threading

import argparse

import sys

import signal
from urwid import MainLoop, Text, AttrMap, Frame, SolidFill, ExitMainLoop

from census import census_repo
from message_board import message_repo
from uberterm_flow import UbertermFlow
from util.killable_thread import KillableThread
from util.pipe_util import wrapped_handler, publish, encode_int


class Uberterm:
    EVENT_SIGINT = 1
    EVENT_INPUT = 2
    EVENT_INACTIVITY = 3

    INACTIVITY_TIMEOUT = 5 * 60  # 5 mins

    __l = logging.getLogger(__name__)

    palette = [
        ('header', 'dark green, standout', 'black'),
        ('content', 'dark green', 'black'),
        ('content_inverse', 'dark green, standout', 'black'),
        ('footer', 'dark green, standout', 'black'),
        ('popup', 'dark green', 'black'),
    ]
    __inactivity_handle = None

    def __init__(self, config_dir):
        self.main_loop = MainLoop(None, self.palette,
                                  input_filter=self.input_filter,
                                  unhandled_input=self.handle_input,
                                  pop_ups=True)
        self.event_pipe = self.main_loop.watch_pipe(wrapped_handler(self.on_event))
        self.config_dir = config_dir
        self.flow = UbertermFlow(self.main_loop, config_dir)

        self.reset_inactivity_alarm()

    def run(self):
        self.__l.info('===== Starting Uberterm =====')
        self.flow.initial()
        try:
            message_repo.initialise(os.path.join(self.config_dir, "messages.db"))
            census_repo.initialise(os.path.join(self.config_dir, "census.csv"))

            self.main_loop.run()
        except BaseException as e:
            self.__l.exception("Unhandled exception")
        finally:
            message_repo.shutdown()

        self.__l.info('===== Exiting Uberterm =====')

        # Clean up any killable_threads
        for thread in threading.enumerate():
            if issubclass(thread.__class__, KillableThread):
                thread.kill()
                thread.join()

        remaining_threads = threading.activeCount()
        if remaining_threads > 1:
            self.__l.warn('Unable to kill all threads, {0} threads still remain'.format(remaining_threads))

    def input_filter(self, keys, raw):
        publish(self.event_pipe, self.EVENT_INPUT)
        return keys

    def handle_input(self, key):
        if type(key) == tuple:
            return  # Ignore all mouse events which come as (event, button, x, y)

        self.__l.debug('handle_input {0}'.format(key))

        if not self.flow.forward_input(key):
            if key == 'esc':
                self.flow.back()

    def handle_sigint(self, signum, frame):
        publish(self.event_pipe, self.EVENT_SIGINT)

    def on_event(self, value):
        if value == self.EVENT_SIGINT:
            self.handle_input("ctrl c")
        if value == self.EVENT_INPUT:
            self.reset_inactivity_alarm()
        if value == self.EVENT_INACTIVITY:
            self.reset_inactivity_alarm()
            self.flow.forward_inactivity()

    def on_inactivity(self, main_loop, user_data):
        self.on_event(self.EVENT_INACTIVITY)

    def reset_inactivity_alarm(self):
        if self.__inactivity_handle:
            self.main_loop.remove_alarm(self.__inactivity_handle)
        self.__inactivity_handle = self.main_loop.set_alarm_in(self.INACTIVITY_TIMEOUT,
                                                               self.on_inactivity)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Uberterm terminal app')
    parser.add_argument('config', help="Uberterm configuration location")
    parser.add_argument('-l', '--log', help='Optional file to log too')
    args = parser.parse_args()

    if args.log is not None:
        logging.basicConfig(filename=args.log, level=logging.DEBUG,
                            format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    else:
        logging.getLogger().setLevel('ERROR')

    if not os.path.exists(args.config):
        os.makedirs(args.config)

    main = Uberterm(args.config)
    signal.signal(signal.SIGINT, main.handle_sigint)
    main.run()
