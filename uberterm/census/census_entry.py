import hashlib


class CensusEntry:
    def __init__(self, name, years, dept, bio, skills, features, rather):
        self.name = name
        self.years = years
        self.dept = dept
        self._bio = bio
        self._skills = skills
        self._features = features
        self.rather = rather

    @property
    def uid(self):
        return hashlib.sha1(self.name.encode('utf-8')).hexdigest()[0:8]

    @property
    def bio(self):
        if self._bio:
            return self._bio
        else:
            return "If a person has no biography do they exist"

    @property
    def skills(self):
        if self._skills:
            return self._skills
        else:
            return "I appears this citizen has no discernible skills"

    @property
    def features(self):
        if self._features:
            return self._features
        else:
            return "This citizen has no features"
