import logging
import time

import urwid
from urwid import Frame, Pile, Divider, Padding, Text, SimpleFocusListWalker, ListBox, Columns, AttrMap, Filler

from census import census_repo
from util.ui.presenter import Presenter
from util.ui.view import View
from widgets import widget_util
from widgets.header import Header
from widgets.listbox_with_scrollbars import ListBoxWithScrollBars
from widgets.plain_button import PlainButton


class CensusDetailsPresenter(Presenter):
    log = logging.getLogger(__name__)

    def __init__(self, view, flow, uid):
        super().__init__(view, flow)
        self.uid = uid

    def on_start(self):
        self.view.entry = census_repo.get_entry(self.uid)


TITLE = "Citizen Census Record of {0}"


class CensusDetailsView(View):
    entry = None

    def __init__(self, main_loop):
        super().__init__(main_loop)

    def _create_body(self):
        if not self.entry:
            raise AssertionError("No census entry")

        content = []
        content.append(Columns([(urwid.FIXED, 8, Text("ÜberID: ")), (urwid.WEIGHT, 1, Text(self.entry.uid))],
                               dividechars=1))
        # content.append(Columns([(urwid.FIXED, 8, Text("Name:   ")), (urwid.WEIGHT, 1, Text(self.entry.name))],
        #                        dividechars=1))
        content.append(Columns([(urwid.FIXED, 8, Text("Dept:   ")), (urwid.WEIGHT, 1, Text(self.entry.dept))],
                               dividechars=1))
        content.append(
            Columns([(urwid.FIXED, 8, Text("Years:  ")), (urwid.WEIGHT, 1, Text(self.entry.years))],
                    dividechars=1))

        content.append(Divider())
        content.append(Text("Bio:"))
        content.append(Text(self.entry.bio))
        content.append(Divider())
        content.append(Text("Special Skills:"))
        content.append(Text(self.entry.skills))
        content.append(Divider())
        content.append(Text("Distinguishing features:"))
        content.append(Text(self.entry.features))
        content.append(Divider())
        content.append(Text("Would you rather be a sparkle pony or a free camper: "))
        content.append(Text(self.entry.rather))

        return Frame(
            header=Header(self.entry.name),
            body=ListBoxWithScrollBars.from_widgets(content),
            footer=Divider())
