import logging
import time

import urwid
from urwid import Frame, Pile, Divider, Padding, Text, SimpleFocusListWalker, ListBox, Columns, AttrMap, Filler

from census import census_repo
from util.ui.presenter import Presenter
from util.ui.view import View
from widgets import widget_util
from widgets.header import Header
from widgets.plain_button import PlainButton


class CensusPresenter(Presenter):
    __l = logging.getLogger(__name__)

    def __init__(self, view, flow):
        super().__init__(view, flow)
        view.on_entry_selected = self.on_entry_selected

    def on_start(self):
        self.view.entries = census_repo.get_entries()

    def on_entry_selected(self, entry):
        self.__l.debug("Entry {0} chosen".format(entry))
        self.flow.launch_census_details(entry.uid)


TITLE = "Übertown Census"


class CensusView(View):
    on_entry_selected = None
    entries = []

    def __init__(self, main_loop):
        super().__init__(main_loop)

    def _create_body(self):
        if self.entries:
            body = ListBox(SimpleFocusListWalker(self.__get_list_content()))
        else:
            body = Filler(Text("No one exists in Ubertown, spooky", align='center'))

        return Frame(
            header=Header(TITLE),
            body=body,
            footer=Divider()
        )

    def _create_footer(self):
        return Text('To select an citizen press <Enter>', align='center')

    def __get_list_content(self):
        content = []

        for entry in self.entries:

            name = PlainButton(entry.name)
            widget_util.hide_button_cursor(name)

            entry_view = Columns([('fixed', 8, Text(entry.uid)),
                                  ('fixed', 16, name),
                                  ('fixed', 30, Text(entry.dept))], dividechars=1)

            name.entry = entry

            def handler(widget):
                if self.on_entry_selected is not None:
                    self.on_entry_selected(widget.entry)

            urwid.connect_signal(name, 'click', handler)
            content.append(AttrMap(entry_view, None, focus_map='content_inverse'))

        return content
