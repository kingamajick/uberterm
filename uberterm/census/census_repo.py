import csv
import logging

import difflib

from census.census_entry import CensusEntry

__l = logging.getLogger(__name__)

__entries = {}

NO_DEPT = "All these options suck, I'm making my own dept (Fill in below)"


def initialise(location):
    with open(location, 'rt', encoding='utf-8') as census_file:
        census_reader = csv.reader(census_file)
        next(census_reader, None)  # Skip headers

        for row in census_reader:
            name = row[1]
            years = row[2]
            dept = row[3]
            if dept == NO_DEPT:
                dept = row[4]
            bio = row[5]
            skills = row[6]
            features = row[7]
            rather = row[8]

            entry = CensusEntry(name, years, dept, bio, skills, features, rather)
            __l.debug("Read entry %s, %s, %s", entry.name, entry.uid, entry.dept)
            __entries[entry.uid] = entry


def shutdown():
    pass


def get_entries():
    return __entries.values()


def get_entry(uid):
    return __entries.get(uid, None)
