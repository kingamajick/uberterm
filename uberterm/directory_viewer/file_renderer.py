from urwid import Frame, Text

from util.ui.presenter import Presenter
from util.ui.view import View
from widgets.header import Header
from widgets.listbox_with_scrollbars import ListBoxWithScrollBars


class FileRendererPresenter(Presenter):
    def __init__(self, path, title, view, flow):
        super().__init__(view, flow)
        self.path = path
        self.title = title

    def on_start(self):
        self.view.title = self.title
        with open(self.path, 'r') as file:
            self.view.content = file.read()


class FileRendererView(View):
    title = ""
    content = ""

    def __init__(self, frame, align='left'):
        super().__init__(frame)
        self.align = align

    def _create_body(self):
        return Frame(
            header=Header(self.title),
            body=ListBoxWithScrollBars.from_widget(Text(self.content, align=self.align))
        )

    def _create_footer(self):
        return Text('Use the arrow keys (↑, ↓), press <ESC> to go back', align='center')
