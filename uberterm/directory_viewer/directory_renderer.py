import os
from collections import namedtuple

import urwid
from urwid import Button, Frame, Pile, Divider, Padding, Text, CENTER, SimpleFocusListWalker, ListBox, RELATIVE, \
    AttrMap, Filler
from util.ui.view import View

from util.ui.presenter import Presenter
from widgets.header import Header
from widgets.widget_util import hide_button_cursor

Node = namedtuple('Node', ['name', 'is_dir'])


class DirectoryRendererPresenter(Presenter):
    def __init__(self, path, title, view, flow):
        super().__init__(view, flow)
        self.path = path
        self.title = title
        self.reader = DirectoryReader(path)
        self.view.on_node_selected = self.on_node_selected

    def on_start(self):
        self.view.title = self.title
        self.view.nodes = self.reader.nodes

    def on_node_selected(self, node):
        path = os.path.join(self.path, node.name)
        if node.is_dir:
            self.flow.launch_directory(path, "{0}/{1}".format(self.title, node.name))
        else:
            self.flow.launch_file(path, "{0}/{1}".format(self.title, node.name))


class DirectoryRendererView(View):
    on_node_selected = None
    title = ""
    nodes = []

    def __init__(self, frame):
        super().__init__(frame)

    def _create_body(self):
        if self.nodes:
            body = Padding(ListBox(SimpleFocusListWalker(self.__get_list_content())), width=(RELATIVE, 80),
                           align=CENTER)
        else:
            body = Filler(Text("Well it seems this is a dead end, press <ESC> to go back"))

        return Frame(
            header=Header(self.title),
            body=body
        )

    def _create_footer(self):
        return Text('Use the arrow keys (↑, ↓)  to select an option then press <Enter>', align='center')

    def __get_list_content(self):
        content = []
        for node in self.nodes:
            button = Button(node.name)
            hide_button_cursor(button)
            button.node = node

            def handler(widget):
                if self.on_node_selected is not None:
                    self.on_node_selected(widget.node)

            urwid.connect_signal(button, 'click', handler)
            content.append(AttrMap(button, None, focus_map='content_inverse'))
        return content


_ORDER_FILE = '.order'


class DirectoryReader:
    def __init__(self, root):
        self.root = os.path.expanduser(root)
        nodes = []

        for (path, dirs, files) in os.walk(self.root):
            nodes.extend(map(lambda d: Node(name=d, is_dir=True), dirs))
            nodes.extend(map(lambda f: Node(name=f, is_dir=False), files))
            break

        sort_from = 0
        # Look for the special .order file
        order_path = os.path.join(self.root, _ORDER_FILE)
        if os.path.isfile(order_path):
            # Remove special .order file
            index = DirectoryReader.find_node_with_name(nodes, _ORDER_FILE)
            del nodes[index]

            # Sort based on the .order file
            order = [line.rstrip('\n') for line in open(order_path)]
            for next_path in order:
                index = DirectoryReader.find_node_with_name(nodes, next_path)
                if index != -1:
                    nodes[sort_from], nodes[index] = nodes[index], nodes[sort_from]
                    sort_from += 1

        # Sort what ever is left.
        nodes[sort_from:len(nodes)] = sorted(nodes[sort_from:len(nodes)], key=lambda n: n.name)

        self.nodes = nodes

    @staticmethod
    def find_node_with_name(nodes, name):
        return next((i for i, node in enumerate(nodes) if node.name == name), -1)
