import logging

import urwid
from urwid import Button, Frame, Pile, Divider, Text, Padding, ListBox, SimpleFocusListWalker, AttrMap
from util.ui.view import View
from util.ui.presenter import Presenter
from widgets.widget_util import hide_button_cursor


class MenuPresenter(Presenter):
    __l = logging.getLogger(__name__)

    options = [
        ('1. Übertown Archives ', 'launch_archives'),
        ('2. Übertown Census ', 'launch_census_list'),
        ('2. Übar Menu', 'launch_bar_menu'),
        ('3. Übertown Message Board ', 'launch_message_board_menu'),
        ("4. Überterm Help", None)
    ]

    def __init__(self, view, flow):
        super().__init__(view, flow)
        view.on_option_selected_callback = self.on_option_selected

    def on_start(self):
        self.view.menu_items = list(map(lambda o: o[0], self.options))

    def handle_input(self, key):
        if super().handle_input(key):
            return True

        if key == 'enter':
            pass

    def on_option_selected(self, option):
        self.__l.debug("Option {0} chosen".format(option))
        if self.options[option][1]:
            getattr(self.flow, self.options[option][1])()
        else:
            self.__l.warn("No option for {0}".format(self.options[option][0]))


_WELCOME = 'Welcome to Überterm citizen, all hail El Présidente, our eternal leader.'


class MenuView(View):
    on_option_selected_callback = None
    menu_items = []

    def __init__(self, main_loop):
        super().__init__(main_loop)

    def _create_body(self):
        return Frame(
            header=Pile(
                [
                    Divider(),
                    Text(_WELCOME, align='center'),
                    Divider(),
                ]
            ),
            body=Padding(ListBox(SimpleFocusListWalker(self.__get_list_content())),
                         width=('relative', 80), align='center'
                         )
        )

    def _create_footer(self):
        return Text('To select an option then press <Enter>', align='center')

    def __get_list_content(self):
        content = []
        for i in range(0, len(self.menu_items)):
            button = Button(self.menu_items[i])
            hide_button_cursor(button)
            button.index = i

            def handler(widget):
                if self.on_option_selected_callback is not None:
                    self.on_option_selected_callback(widget.index)

            urwid.connect_signal(button, 'click', handler)
            content.append(AttrMap(button, None, focus_map='content_inverse'))
        return content
