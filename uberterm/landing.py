from urwid import Filler, Pile, Padding, Text, Divider, LineBox
from util.ui.view import View

from util.ui.presenter import Presenter


class LandingPresenter(Presenter):
    def __init__(self, view, flow):
        super().__init__(view, flow)

    def handle_input(self, key):
        if super().handle_input(key):
            return True

        if key == 'enter':
            self.flow.launch_menu()

    def handle_inactivity(self):
        pass  # Don't care about users been inactive here.


LOGO = """
(_) (_) |__   ___ _ __| |_ ___ _ __ _ __ ___  
| | | | '_ \ / _ \ '__| __/ _ \ '__| '_ ` _ \ 
| |_| | |_) |  __/ |  | ||  __/ |  | | | | | |
 \___/|_.__/ \___|_|   \__\___|_|  |_| |_| |_|
"""

LOGO2 = """
   (_) (_)/ __ ) / ____// __ \ 
  / / / // __  |/ __/  / /_/ /
 / /_/ // /_/ // /___ / _, _/ 
 \____//_____//_____//_/ |_|
             INFOTERMINAL 3000"""

WARNING_TITLE = "WARNING"
WARNING_TEXT = """
Unauthorised access to the Überterm is strictly and absolutely forbidden.

Disconnect IMMEDIATELY if you are not an authorized user
""".rstrip('\n')  # Remove new lines

PRESS_ENTER = "Press <ENTER> to continue"


class LandingView(View):
    def __init__(self, main_loop):
        super().__init__(main_loop)

    def _create_body(self):
        return Filler(Pile(
            [
                Padding(Text(LOGO2), align='center', width='pack'),
                Divider(),
                Padding(
                    LineBox(
                        Padding(
                            Text(WARNING_TEXT, align='center'),
                            width=41, align='center'),
                        title=WARNING_TITLE),
                    width=('relative', 80), align='center'),
            ]
        ), valign='top')

    def _create_footer(self):
        return Text(PRESS_ENTER, align='center')
