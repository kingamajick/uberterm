from urwid import WidgetWrap, Divider, Text, ListBox, SimpleFocusListWalker, Frame, SolidFill, Columns


class ListBoxWithScrollBars(WidgetWrap):
    __showing_up_arrow = False
    __showing_down_arrow = False
    __blank = Divider()
    __up_arrow = Text("↑")
    __down_arrow = Text("↓")

    def __init__(self, list_box):
        self.__wrapped_list_box = list_box
        self.scroll_bar = Frame(header=self.__blank, body=SolidFill(" "), footer=self.__blank)
        composite = Columns([('weight', 1, self.__wrapped_list_box),
                             ('fixed', 1, self.scroll_bar)])
        super().__init__(composite)

    @classmethod
    def from_widget(cls, widget):
        return cls(ListBox(SimpleFocusListWalker([widget])))

    @classmethod
    def from_widgets(cls, widgets):
        return cls(ListBox(SimpleFocusListWalker(widgets)))

    @classmethod
    def from_walker(cls, walker):
        return cls(ListBox(walker))

    @classmethod
    def from_list_box(cls, list_box):
        return cls(list_box)

    @property
    def list(self):
        return self.__wrapped_list_box

    def render(self, size, focus=False):
        (max_col, max_row) = size
        ends_visible = self.__wrapped_list_box.ends_visible((max_col - 1, max_row))

        top_visible = 'top' in ends_visible

        if top_visible and self.__showing_up_arrow:
            self.__showing_up_arrow = False
            self.scroll_bar.header = self.__blank
        if not top_visible and not self.__showing_up_arrow:
            self.__showing_up_arrow = True
            self.scroll_bar.header = self.__up_arrow

        bottom_visible = 'bottom' in ends_visible

        if bottom_visible and self.__showing_down_arrow:
            self.__showing_down_arrow = False
            self.scroll_bar.footer = self.__blank
        if not bottom_visible and not self.__showing_down_arrow:
            self.__showing_down_arrow = True
            self.scroll_bar.footer = self.__down_arrow

        return super().render(size, focus)
