import logging
from urwid import ListBox


class TabaleListBox(ListBox):
    def __init__(self, body):
        super().__init__(body)
        self.__l = logging.getLogger(self.__class__.__name__)

    def keypress(self, size, key):
        super().keypress(size, key)
        if not key:
            return

        __, position = self.get_focus()
        self.__l.debug('Current position {0}'.format(position))

        if key == 'tab':
            widget, position = self.__get_next_focus(position, self.body.get_next)
            self.__l.debug('Moving to next {0}'.format(position))
        elif key == 'shift tab':
            widget, position = self.__get_next_focus(position, self.body.get_prev)
            self.__l.debug('Moving to prev {0}'.format(position))
        else:
            return key

        self.__l.debug('Moving  {0} : {1}'.format(widget, position))
        if position:
            self.set_focus(position)
        else:
            return key

    @staticmethod
    def __get_next_focus(position, next_func):
        widget, position = next_func(position)
        while widget:
            if widget.selectable():
                return widget, position
            widget, position = next_func(position)
