import types


def hide_button_cursor(button):
    """
    Hacks uriwd.button to remove the cursor
    :param button: 
    :return: 
    """

    # noinspection PyUnusedLocal
    def get_cursor_coords(_, __, ___=None):
        return None

    # noinspection PyArgumentList,PyArgumentList,PyProtectedMember
    button._label.get_cursor_coords = types.MethodType(get_cursor_coords, button._label)
