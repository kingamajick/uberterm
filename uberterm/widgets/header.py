from urwid import WidgetWrap, Pile, Divider, Padding, Text, CENTER


class Header(WidgetWrap):
    def __init__(self, title):
        super().__init__(Pile(
            [
                Divider(),
                Padding(Text(title, align=CENTER), left=2, right=2),
                Divider()
            ]))