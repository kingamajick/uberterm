import urwid
from urwid import Button, Text


class PlainButton(Button):

    button_left = Text("")
    button_right = Text("")

    def __init__(self, label, on_press=None, user_data=None):
        super().__init__(label, on_press, user_data)
