import os
import re

from time import sleep

import urwid
from urwid import Text, decompose_tagmarkup

from util.killable_thread import KillableThread
from util.pipe_util import encode_int, decode_int


class TypeWriterText(Text):
    """
    A text widget providing type writer style text (text that appears a single character at a time)
    """
    __thread = None
    __pattern = re.compile('\S')
    text = None
    kill = False

    def __init__(self, text, main_loop):
        self.__pipe = main_loop.watch_pipe(self.__update)
        super().__init__(text, urwid.LEFT, urwid.SPACE)

    def show_complete_text(self):
        """
        Shows all the text previously set by set_text
        :return: True is this cancelled the type writer mode, False if the type writer mode had completed and all the
        text was currently displayed
        """
        if self.__thread:
            self.__thread.end = True
            self.__thread = None
            super().set_text(self.text)
            self._invalidate()
            return True

        return False

    def cancel_type_writer(self):
        """

        :return: True is this cancelled the type writer mode, False if the type writer mode had completed and all the
        text was currently displayed
        """
        if self.__thread:
            self.__thread.kill = True
            self.__thread = None
            return True

        return False

    def set_text(self, markup):
        self.text, self._attrib = decompose_tagmarkup(markup)
        # Use whitespace to ensure the size is the same if the actual text was displayed.
        self._text = self.__pattern.sub(' ', self.text)

        if self.__thread:
            self.__thread.kill = True

        self.__thread = TypeWriterText.OutputThread(self.__pipe, len(self.text))
        self.__thread.start()

    def __update(self, value):
        if not self.__thread:
            return

        decoded = decode_int(value)
        super().set_text(self.text[:decoded] + self._text[decoded:])
        self._invalidate()

    class OutputThread(KillableThread):
        __kill = False

        def __init__(self, pipe, length):
            super().__init__()
            self.pipe = pipe
            self.length = length

        def run(self):
            for i in range(0, self.length):
                if self._kill:
                    return

                self.publish(i)
                sleep(0.06)

        def publish(self, value):
            encoded = encode_int(value)
            os.write(self.pipe, encoded)
