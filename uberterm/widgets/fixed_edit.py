import sys

from urwid import Edit, SPACE, LEFT


class FixedEdit(Edit):
    def __init__(self, caption="", edit_text="", multiline=False,
                 align=LEFT, wrap=SPACE, allow_tab=False,
                 edit_pos=None, layout=None, mask=None, max_length=sys.maxsize):
        super().__init__(caption, edit_text, multiline,
                         align, wrap, allow_tab,
                         edit_pos, layout, mask)
        self.max_length = max_length

    def keypress(self, size, key):
        if self.valid_char(key) and len(self.edit_text) > self.max_length - 1:
            return None
        else:
            return super().keypress(size, key)
